﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class StickHeroController : MonoBehaviour
{
    [SerializeField] private StickHeroStick m_Stick;
    [SerializeField] private StickHeroPlayer m_Player;
    [SerializeField] private StickHeroPlatform[] m_Platforms;

    private int counter;//это счетчик платформ
    private int scoreCounter;
    
    public enum EGameState
    {
        Wait,
        Scaling,
        Rotate,
        Movement,
        Defeat,
    }

    private EGameState currentGameState;
    
    // Start is called before the first frame update
    private void Start()
    {
        currentGameState = EGameState.Wait;
        counter = 0;
        scoreCounter = 0;
            
        m_Stick.ResetStick(m_Platforms[0].GetStickPosition());
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) == false)
        {
            return;
        }

        switch (currentGameState)
        {
            //если не осуществлен старт игры
            case EGameState.Wait:
                currentGameState = EGameState.Scaling;
                m_Stick.StartScaling();
                break;
            // стик увеличивается - прерываем увеличение и запускаем поворот
            case EGameState.Scaling:
                currentGameState = EGameState.Rotate;
                m_Stick.StopScaling();
                break;
            
            //ничего не делаем
            case EGameState.Rotate:
                break;
            
            //ничего не делаем
            case EGameState.Movement:
                break;
            
            case EGameState.Defeat:
                ShowScores();
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;
            
            default:
                throw new ArgumentOutOfRangeException();
        }
        
    }

    public void StopStickScale()
    {
        currentGameState = EGameState.Rotate;
        m_Stick.StartRotate();
    }

    public void StopStickRotate()
    {
        currentGameState = EGameState.Movement;
    }

    public void StartPlayerMovement(float length)
    {
        currentGameState = EGameState.Movement;
        int counterPlus;
        if (counter == 9) counterPlus = 0;
        else counterPlus = counter + 1;
        StickHeroPlatform nextPlatform = m_Platforms[counterPlus];
        
        //находим минимальную длину стика для успешного перехода
        float targetLength = 
            nextPlatform.transform.position.x - m_Stick.transform.position.x;

        float platformSize = nextPlatform.GetPlatformSize();

        float min = targetLength - platformSize * 0.5f;
        min -= m_Player.transform.localScale.x * 0.9f;
        
        //находим максимальную длину стика для успешного перехода
        float max = targetLength + platformSize * 0.5f;
        
        //при успехе переъодим в центр следующей платформы, иначе падаем

        if (length < min || length > max)
        {
            //будем падать
            float targetPosition = m_Stick.transform.position.x + length +
                                   m_Player.transform.localScale.x;
            
            m_Player.StartMovement(targetPosition, true);
        }
        else
        {
            float targetPosition = nextPlatform.transform.position.x;
            m_Player.StartMovement(targetPosition, false);
            if(scoreCounter>3) PlatformMovement(counterPlus);
        }

    }
    
    public void PlatformMovement(int platformMovNumber)//двигает платформу
    {
        if (platformMovNumber >= 5) platformMovNumber= platformMovNumber-5; else platformMovNumber = platformMovNumber+5;
        StickHeroPlatform movedPlatform = m_Platforms[platformMovNumber];
        
        //номер платформы, от которой рассчитываеся расстояние до будущей предыдущей платформы
        int prevPlatformNumber=platformMovNumber;
        if (prevPlatformNumber!=0) prevPlatformNumber--; else prevPlatformNumber=9;

        movedPlatform.transform.localScale = new Vector3(Random.Range(0.2f, 0.7f), 1f,1f);
        
        var newXPosition = -movedPlatform.transform.position.x +m_Platforms[prevPlatformNumber].transform.position.x+ m_Platforms[prevPlatformNumber].transform.localScale.x/2 + movedPlatform.transform.localScale.x/2+ Random.Range(0.2f,0.7f);
        movedPlatform.transform.Translate(new Vector3(newXPosition, 0.0f, 0.0f), Space.World);

    }
    
    public void StopPlayerMovement()
    {
        currentGameState = EGameState.Wait;
        counter++;
        if (counter == 10) counter = 0;
        scoreCounter++;
        m_Stick.ResetStick(m_Platforms[counter].GetStickPosition());
    }

    public void ShowScores()
    {
        currentGameState = EGameState.Defeat;
        
        print($"Game over at {scoreCounter}");
    }
    void OnGUI()
    {
        // Make a multiline text area that modifies stringToEdit.
        GUI.TextArea(new Rect(10, 10, 100, 22), $"Счёт: {scoreCounter}", 200);
    }
    
}
