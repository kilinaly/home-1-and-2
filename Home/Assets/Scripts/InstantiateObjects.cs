﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class InstantiateObjects : MonoBehaviour
{
    [SerializeField] private GameObject m_RedFigure;
    [SerializeField] private GameObject m_BlueFigure;
    [SerializeField] private GameObject m_GreenFigure;
    [SerializeField] private GameObject m_YellowFigure;
    [SerializeField] private GameObject m_PurpleFigure;

    int counterGameOver;//количество блоков в высоту для проигрыша
    private int figureNumber = 0;//номер фигуры
    
    //счётчики для остановки движения;
    private double counterMoveDownZ = 0;
    private double counterMoveDownY = 0;
    
    private bool needMoveInUpPosition=true;//нужно ли MoveInUpPosition
    private bool needMoveBack = false;
    private bool needMoveDown = false;
    private bool firstMoveBack = true;

    void Update()
    {
        if (counterGameOver<10)
        {
            switch (figureNumber)
            {
                case 0:
                    if(needMoveInUpPosition)MoveInUpPosition(m_RedFigure);
                    if(needMoveBack)MoveBack(m_GreenFigure);
                    if(needMoveDown)MoveDown(m_RedFigure, 3,0, -0.3); 
                    break;
                
                 case 1:
                     if(needMoveInUpPosition)MoveInUpPosition(m_GreenFigure);
                     if(needMoveBack)MoveBack(m_BlueFigure);
                     if(needMoveDown)MoveDown(m_GreenFigure,1,0,0);
                     break;
                 
                case 2:
                    if(needMoveInUpPosition)MoveInUpPosition(m_BlueFigure);
                    if(needMoveBack)MoveBack(m_YellowFigure);
                    if(needMoveDown)MoveDown(m_BlueFigure,3,1,0);
                    break;
                
                case 3:
                    if(needMoveInUpPosition)MoveInUpPosition(m_YellowFigure);
                    if(needMoveBack)MoveBack(m_PurpleFigure);
                    if(needMoveDown)MoveDown(m_YellowFigure,3,1,0);
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
    }

    private void MoveDown(GameObject colorFigure, int numberOfBlocks, int debug, double debug2)//numberOfBlocks - количество блоков в фигуре в высоту
    {
        if (counterMoveDownZ>-0.3+debug2)
        {
            counterMoveDownZ -=Time.deltaTime*0.2;
            colorFigure.transform.Translate(Vector3.back * Time.deltaTime*0.2f);
        }
        else
        {
            if (counterMoveDownY > counterGameOver - 8 + debug)
            {
                counterMoveDownY -=Time.deltaTime;
                colorFigure.transform.Translate(Vector3.down * Time.deltaTime);
            }
            else
            {
                counterGameOver += numberOfBlocks;
                figureNumber++;
                counterMoveDownZ = 0;
                counterMoveDownY = 0;
                needMoveDown = false;
                firstMoveBack = true;
                needMoveInUpPosition = true;
            }
        }
    }

    private void MoveInUpPosition( GameObject colorFigure)//перемещение к начальной позиции перед спуском
    {
        colorFigure.transform.Translate(new Vector3(-8f, 3.0f, 0.3f));
        needMoveInUpPosition = false;
        needMoveBack = true;
    }

    private void MoveBack(GameObject colorFigure)
    {
        if (counterMoveDownZ>-0.3)
             {
                 counterMoveDownZ -=Time.deltaTime*0.2f;
                 colorFigure.transform.Translate(Vector3.back * Time.deltaTime*0.2f);
             }
             else
             {
                 if (firstMoveBack)
                 {
                     counterMoveDownZ = 0;
                     needMoveDown = true;
                     firstMoveBack=false;
                     needMoveBack = false;
                 }
             }
         }

}

